import './styles/main.sass'

import React from 'react';
import ReactDOM from 'react-dom/client';
import {CitiesInput} from "./inputs/cities";
import {test3} from "./test3";

const root = ReactDOM.createRoot(
  document.getElementById('root')
);

root.render(
  <>
    <CitiesInput />
  </>
);

test3()