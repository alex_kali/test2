export const citiesRequest = async (value, callback) => {
  const requestTime = Date.now();
  window.lastRequestTime = requestTime;
  $.kladr.api({
    query: value,
    type: 'city',
    limit: 2,
    withParent: 0
  }, function(responce){
    if(window.lastRequestTime === requestTime){
      responce.shift(); callback(responce);
    }
  })
}
