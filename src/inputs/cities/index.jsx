import {useEffect, useRef, useState} from "react";
import {citiesRequest} from "./request";
import { useOutsideClick } from "rooks";

export const CitiesInput = () => {
  const [city, setCity] = useState('')
  const [cityOptions, setCityOptions] = useState([])
  const [isViewOptions, setIsViewOptions] = useState(true)
  const ref = useRef()

  useOutsideClick(ref, () => {setIsViewOptions(false)});

  useEffect(()=>{
    if(!city){
      setCityOptions([])
      window.lastRequestTime = undefined
    }
  }, [city])

  return (
    <div className={"cities"} ref={ref}>
      <input
        className={"cities__input"}
        value={city}
        onChange={(e) => {
          if(e.target.value){
            citiesRequest(e.target.value,setCityOptions);
          }
          setCity(e.target.value)}
        }
        onFocus={()=> setIsViewOptions(true)}
      />

      {cityOptions && city && isViewOptions && (
        <div className={"cities__list"}>
          {cityOptions.map((item)=>
            <div key={item.id} className={"cities__item"} onClick={()=>{
              setCity(item.name)
              setCityOptions([])
              window.lastRequestTime = undefined
            }}

            >{item.name}</div>
          )}
        </div>
      )}
    </div>
  )
}