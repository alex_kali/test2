const case1 = {
  name1: '',
  name2: {
    name3: '',
    name4: '',
  },
  name10: [
    '',
    '',
    {
      name5: '',
      name6: '',
      name7: {
        name8: '',
        name9: ''
      }
    }
  ],
}

export const test3 = () => {

  const toUpperCase = (item) => {
    const isArrayItem = Array.isArray(item)

    for(let i in item){

      if(typeof(item[i]) === 'object'){
        item[i] = toUpperCase(item[i])
      }

      if(!isArrayItem){
        if(i.toUpperCase() !== i){
          item[i.toUpperCase()] = item[i]
          delete item[i]
        }
      }
    }
    return item
  }

  console.log(toUpperCase(case1))
}